# Andrew Ridgway Tidbits

I am going to use this repo to store the little bits of code that get built but never
quite used. this will allow for a central repo of 'stuff' that I can call on 

## Items

### pydeequ
The pydeequ folder contains a notebook and dependancies to create a _pydeequ glue job builder_ it can be pointed at a text file and it will build a glue job based on the results
It requires a glue dev endpoint set up with the dependancies listed in the _dependancies_ folder (you will find a jar as well as python library which need to be added to the endpoint/job)

